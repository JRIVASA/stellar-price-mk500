﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Stellar_isPRICE
{

    public partial class sinConexion : Form
    {
        public sinConexion()
        {
            InitializeComponent();
        }

        private void timerConexion_Tick(object sender, EventArgs e)
        {
            timerConexion.Enabled = false;
            this.Close();
        }
    }

}