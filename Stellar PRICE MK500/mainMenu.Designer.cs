﻿namespace isPRICE
{
    partial class mainMenu
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainMenu));
            this.probarConexion = new System.Windows.Forms.Button();
            this.codigoBox = new System.Windows.Forms.TextBox();
            this.wallpaperBox = new System.Windows.Forms.PictureBox();
            this.flechaBox = new System.Windows.Forms.PictureBox();
            this.flechaTimer = new System.Windows.Forms.Timer();
            this.ImgTimer = new System.Windows.Forms.Timer();
            this.SuspendLayout();
            // 
            // probarConexion
            // 
            this.probarConexion.BackColor = System.Drawing.Color.Transparent;
            this.probarConexion.Enabled = false;
            this.probarConexion.Location = new System.Drawing.Point(13, 3);
            this.probarConexion.Name = "probarConexion";
            this.probarConexion.Size = new System.Drawing.Size(90, 24);
            this.probarConexion.TabIndex = 1;
            this.probarConexion.Text = "Conexion BD";
            this.probarConexion.Visible = false;
            this.probarConexion.Click += new System.EventHandler(this.probarConexion_Click);
            // 
            // codigoBox
            // 
            this.codigoBox.BackColor = System.Drawing.Color.White;
            this.codigoBox.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.codigoBox.Enabled = false;
            this.codigoBox.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold);
            this.codigoBox.Location = new System.Drawing.Point(54, 69);
            this.codigoBox.Name = "codigoBox";
            this.codigoBox.Size = new System.Drawing.Size(215, 21);
            this.codigoBox.TabIndex = 2;
            this.codigoBox.Visible = false;
            this.codigoBox.TextChanged += new System.EventHandler(this.codigoBox_TextChanged);
            this.codigoBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.codigoBox_KeyDown);
            // 
            // wallpaperBox
            // 
            this.wallpaperBox.Image = ((System.Drawing.Image)(resources.GetObject("wallpaperBox.Image")));
            this.wallpaperBox.Location = new System.Drawing.Point(0, 0);
            this.wallpaperBox.Name = "wallpaperBox";
            this.wallpaperBox.Size = new System.Drawing.Size(320, 240);
            this.wallpaperBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // flechaBox
            // 
            this.flechaBox.Image = ((System.Drawing.Image)(resources.GetObject("flechaBox.Image")));
            this.flechaBox.Location = new System.Drawing.Point(0, 0);
            this.flechaBox.Name = "flechaBox";
            this.flechaBox.Size = new System.Drawing.Size(320, 240);
            this.flechaBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // flechaTimer
            // 
            this.flechaTimer.Enabled = true;
            this.flechaTimer.Interval = 1000;
            this.flechaTimer.Tick += new System.EventHandler(this.flechaTimer_Tick);
            // 
            // ImgTimer
            // 
            this.ImgTimer.Enabled = true;
            this.ImgTimer.Interval = 10000;
            this.ImgTimer.Tick += new System.EventHandler(this.ImgTimer_Tick);
            // 
            // mainMenu
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(320, 240);
            this.ControlBox = false;
            this.Controls.Add(this.probarConexion);
            this.Controls.Add(this.codigoBox);
            this.Controls.Add(this.flechaBox);
            this.Controls.Add(this.wallpaperBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "mainMenu";
            this.Text = "Stellar PRICE - www.bigwise.com";
            this.Deactivate += new System.EventHandler(this.mainMenu_Deactivate);
            this.Load += new System.EventHandler(this.mainMenu_Load);
            this.Activated += new System.EventHandler(this.mainMenu_Activated);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button probarConexion;
        private System.Windows.Forms.TextBox codigoBox;
        private System.Windows.Forms.PictureBox wallpaperBox;
        private System.Windows.Forms.PictureBox flechaBox;
        private System.Windows.Forms.Timer flechaTimer;
        private System.Windows.Forms.Timer ImgTimer;
    }
}