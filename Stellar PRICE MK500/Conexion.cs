﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace isPRICE
{
    class Conexion
    {
        //C# no acepta \, debe usarse \\ o @ antes del string.
        static string BD = "VAD20";
        static string Server = @"192.168.28.194\CLIENTE06";
        static string url = "Data Source=" + Server + ";Initial Catalog=" + BD + ";User ID=sa";
        static SqlConnection connection = new SqlConnection(url);
        static string testConsulta = "SELECT c_descri FROM ma_productos WHERE C_CODIGO = 3427643";
        static SqlDataAdapter da = new SqlDataAdapter(testConsulta, connection);
        static DataTable dt = new DataTable();

        public void consulta()
        {
            try
            {
                dt.Clear();
                connection.Open();
                int recordsAffected = da.Fill(dt);
                if (recordsAffected > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        //MessageBox.Show(dr[0].ToString());
                        MessageBox.Show("Conexion establecida", "Prueba de Conexion", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                    }
                }
                else
                {
                    MessageBox.Show("Error", "Prueba de conexion", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
                }
            }
            catch (SqlException e)
            {
                MessageBox.Show(e.Message, "Prueba de conexion", MessageBoxButtons.OK, MessageBoxIcon.Hand, MessageBoxDefaultButton.Button1);
            }
            finally
            {
                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }
            }
        }
    }
}
