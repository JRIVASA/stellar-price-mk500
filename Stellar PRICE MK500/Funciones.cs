﻿using System;
//using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Drawing;

namespace isPRICE
{

    public class Funciones
    {

        public Boolean DebugMode;
        public Boolean TestMode;

        public String FormDesignHeight = String.Empty;
        public String FormDesignWidth = String.Empty;
        public String CurrentScreenWidth = String.Empty;
        public String CurrentScreenHeight = String.Empty;

        public Double CurrentHeightFactor = 1;
        public Double CurrentWidthFactor = 1;
        public Double CurrentFontFactor = 1;

        public Double RuntimeVSDesignRatioX = 1;
        public Double RuntimeVSDesignRatioY = 1;

        private const Double mFactorAjustarFuente = 0.875;

        // Variables Globales

        public String BD_ADM;
        public String BD_POS;

        public String Precios_ConsultaMonedaAlterna = String.Empty;
        public String Precios_ConsultaMonedaAlterna2 = String.Empty;
        public Boolean Precios_ConsultaMonedaAlterna_RedondearHaciaArriba = true;
        public Int32 Precios_ConsultaMonedaAlterna_DecimalesPantalla = -1;
        public Int32 Precios_ConsultaMonedaAlterna2_DecimalesPantalla = -1;

        public String Moneda_Cod;
        public String Moneda_Des;
        public String Moneda_Sim;
        public Double Moneda_Fac;
        public Int32 Moneda_Dec;

        public Boolean MostrarPrecioAlterno = false;
        public Boolean MostrarPrecioAlterno2 = false;
        public Double PrecioFinalAlt;
        public Double PrecioFinalAlt2;
        public Double PrecioFicha;
        public Double PrecioOfertaFicha;
        
        public String MonedaAlt_Cod;
        public String MonedaAlt_Des;
        public String MonedaAlt_Sim;
        public Double MonedaAlt_Fac;
        public Int32 MonedaAlt_Dec;

        public String MonedaAlt2_Cod;
        public String MonedaAlt2_Des;
        public String MonedaAlt2_Sim;
        public Double MonedaAlt2_Fac;
        public Int32 MonedaAlt2_Dec;

        public Boolean FactorizandoPrecios;
        public Int32 ImpresoraFiscal_DecimalesPrecio;
        public String TipoPrecioMostrar;

        public Int32 CantDec;
        public Int32 DecMonedaProd;

        private void AjustarFuente(Control Ctrl, Double Factor)
        {

            try
            {
                Ctrl.Font = new Font(Ctrl.Font.Name, (float)(Ctrl.Font.Size * Factor), Ctrl.Font.Style);
            }
            catch (Exception Any)
            {
                Console.WriteLine(Any.Message);
            }

        }

        private void AjustarPantalla(Form TargetForm, Int32 XSize, Int32 YSize)
        {
            
            try
            {

            TargetForm.Top = 0;

            TargetForm.Left = 0;

            if (DebugMode) { 
                MessageBox.Show("Form Original Height: " + TargetForm.Height.ToString() + "\n" + 
                "Form Original Width: " + TargetForm.Width.ToString() + "\n" + 
                "Intended Height: " + YSize.ToString() + "\n" + 
                "Intended Width: " + XSize.ToString() + "\n" + 
                "Height Ratio: " + (YSize / Conversion.Val(FormDesignHeight)).ToString() + "\n" + 
                "Width Ratio: " + (XSize / Conversion.Val(FormDesignWidth)).ToString());
            }
            
            TargetForm.Height = YSize;

            TargetForm.Width = XSize;

            TargetForm.MinimizeBox = false;
            TargetForm.MaximizeBox = false;
            TargetForm.AutoScaleMode = AutoScaleMode.Dpi;
            TargetForm.AutoScroll = true;
            TargetForm.FormBorderStyle = FormBorderStyle.None;

            Double HeightRatio;
            Double WidthRatio;
            Double FontFactor;

            HeightRatio = (YSize / Conversion.Val(FormDesignHeight));
            WidthRatio = (XSize / Conversion.Val(FormDesignWidth));
            FontFactor = (mFactorAjustarFuente * ((HeightRatio + WidthRatio) / 2));

            CurrentHeightFactor = HeightRatio;
            CurrentWidthFactor = WidthRatio;
            CurrentFontFactor = FontFactor;

            for (Int32 i = 0; i <= TargetForm.Controls.Count - 1; i++)
            {

                TargetForm.Controls[i].Left = (Int32) (TargetForm.Controls[i].Left * WidthRatio);

                TargetForm.Controls[i].Top = (Int32) (TargetForm.Controls[i].Top * HeightRatio);

                TargetForm.Controls[i].Width = (Int32) (TargetForm.Controls[i].Width * WidthRatio);

                TargetForm.Controls[i].Height = (Int32)(TargetForm.Controls[i].Height * HeightRatio);

                AjustarFuente(TargetForm.Controls[i], FontFactor);

            }

            } catch(Exception Any)
            {
                Console.WriteLine(Any.Message);
            }

        }

        private void AjustarPantalla(Panel TargetForm, Int32 XSize, Int32 YSize)
        {

            try
            {

                TargetForm.Top = 0;

                TargetForm.Left = 0;

                TargetForm.Height = YSize;

                TargetForm.Width = XSize;

                TargetForm.AutoScroll = true;

                Double HeightRatio;
                Double WidthRatio;
                Double FontFactor;

                HeightRatio = (YSize / Conversion.Val(FormDesignHeight));
                WidthRatio = (XSize / Conversion.Val(FormDesignWidth));
                FontFactor = (mFactorAjustarFuente * ((HeightRatio + WidthRatio) / 2));

                for (Int32 i = 0; i <= TargetForm.Controls.Count - 1; i++)
                {

                    TargetForm.Controls[i].Left = (Int32)(TargetForm.Controls[i].Left * WidthRatio);

                    TargetForm.Controls[i].Top = (Int32)(TargetForm.Controls[i].Top * HeightRatio);

                    TargetForm.Controls[i].Width = (Int32)(TargetForm.Controls[i].Width * WidthRatio);

                    TargetForm.Controls[i].Height = (Int32)(TargetForm.Controls[i].Height * HeightRatio);

                    AjustarFuente(TargetForm.Controls[i], FontFactor);

                }

            }
            catch (Exception Any)
            {
                Console.WriteLine(Any.Message);
            }

        }

        public void AjustarFormularios(Form TargetForm)
        {

            if (!(Information.IsNumeric(FormDesignHeight) 
            && Information.IsNumeric(FormDesignWidth) 
            && Information.IsNumeric(CurrentScreenHeight) 
            && Information.IsNumeric(CurrentScreenWidth)))
            {
                CurrentHeightFactor = 1;
                CurrentWidthFactor = 1;
                CurrentFontFactor = 1;
                return;
            }else{
                AjustarPantalla(TargetForm, (Int32) Conversion.Val(CurrentScreenWidth), (Int32) Conversion.Val(CurrentScreenHeight));
            }

        }

        public void AjustarFormularios(Panel TargetForm)
        {

            if (!(Information.IsNumeric(FormDesignHeight)
            || Information.IsNumeric(FormDesignWidth)
            || Information.IsNumeric(CurrentScreenHeight)
            || Information.IsNumeric(CurrentScreenWidth)))
            {
                CurrentHeightFactor = 1;
                CurrentWidthFactor = 1;
                CurrentFontFactor = 1;
                return;
            }
            else
            {
                AjustarPantalla(TargetForm, (Int32)Conversion.Val(CurrentScreenWidth), (Int32)Conversion.Val(CurrentScreenHeight));
            }

        }
        
        public Int32 RoundVerifica(Int32 pVar)
        {
            String Valido, Cal, Fal;
            Valido =  Strings.Left(Strings.Right(pVar.ToString(), 2),1);
            Valido = Conversion.Str(Conversion.Val(Valido) + 1);
            Cal = Strings.Replace(pVar.ToString(), Strings.Right(pVar.ToString(), 2), String.Empty, 1, 1, CompareMethod.Text);
            Fal = Cal + Valido + "0";
            return Convert.ToInt32(Fal);
        }

        public Boolean VerificarDigito(String Codigo)
        {
            
            Int32 Par, ImPar, A, Z;
            Int32 SL, SS;

            A = 0;
            Z = 0;
            Par = 0;
            ImPar = 0;

            for (int i = 0; i < Codigo.Length - 1; i++)
			{

			    SS = i;
                SL = 1;

                A = Convert.ToInt32(Codigo.Substring(SS, SL));

                Z = i + 1;
                
                if ((Z % 2) == 0)
                    Par = Par + A;
                else
                    ImPar = ImPar + A;

			}

            Par = (Par * 3) + ImPar;
            A = RoundVerifica(Par);
            Par = (A - Par);

            SS = Codigo.Length - 1;
            SL = 1;
            
            if (Conversion.Val(Codigo.Substring(SS, SL)) == Conversion.Val(Strings.Right(Par.ToString(), 1)))
                return true;
            else
                return false;

        }

        public String FixTSQL(String pText)
        {
            return pText.Replace("'", "''");
        }

    }

}
