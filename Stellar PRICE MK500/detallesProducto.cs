﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using isPRICE;

namespace Stellar_isPRICE
{
    public partial class detallesProducto : Form
    {

        private Boolean FormaCargada = false;

        public detallesProducto()
        {
            try
            {
                
                InitializeComponent();

                FormaCargada = false;

                Variables var = new Variables();
                timer1.Enabled = true;

                String ExpresionMoneda = String.Empty;

                if (!String.IsNullOrEmpty(var.SimMoneda))
                {
                    ExpresionMoneda = " " + var.SimMoneda;
                }

                ivaLabel.Text = var.LabelTaxID + ":";
                precioLabel.Text = var.LabelPrice + ":";
                ofertaLabel.Text = var.LabelPriceOffer + ":";
                totalLabel.Text = var.LabelTotal + ":";
                desdeLabel.Text = var.LabelFrom + ":";
                hastaLabel.Text = var.LabelUntil + ":";
                
                if (var.VarMostrarProductImgNotFound)
                    productoBox.Visible = true;
                else
                    productoBox.Visible = false;

                String mDecimales; Int32 mDecimalesInt;

                if (mainMenu.fn.FactorizandoPrecios)
                {
                    mDecimalesInt = mainMenu.fn.Moneda_Dec;
                    mDecimales = (mDecimalesInt == 0 ? String.Empty : "." + String.Empty.PadRight(mDecimalesInt, '0'));
                }
                else
                {
                    mDecimalesInt = mainMenu.fn.DecMonedaProd;
                    mDecimales = (mDecimalesInt == 0 ? String.Empty : "." + String.Empty.PadRight(mDecimalesInt, '0'));
                }

                if (var.TieneOf)
                {
                    limpiar();
                    descripcionBox.Text = var.Descri;
                    precioBox.Text = var.Subtotal.ToString("#,###,###,###,##0" + mDecimales) + ExpresionMoneda;
                    ofertaBox.Text = var.PrecioOfe.ToString("#,###,###,###,##0" + mDecimales) + ExpresionMoneda;
                    ivaBox.Text = var.Impue.ToString("#,###,###,###,##0" + mDecimales) + ExpresionMoneda;
                    totalBox.Text = var.TotalOfe.ToString("#,###,###,###,##0" + mDecimales) + ExpresionMoneda;
                    desdeBox.Text = var.FechaIni.ToShortDateString(); //.ToString();
                    hastaBox.Text = var.FechaLimi.ToShortDateString();
                }
                else
                {
                    limpiar();
                    ocultarOferta();
                    descripcionBox.Text = var.Descri;
                    precioBox.Text = var.Subtotal.ToString("#,###,###,###,##0" + mDecimales) + ExpresionMoneda;
                    ivaBox.Text = var.Impue.ToString("#,###,###,###,##0" + mDecimales) + ExpresionMoneda;
                    totalBox.Text = var.Prec.ToString("#,###,###,###,##0" + mDecimales) + ExpresionMoneda;
                }

                if (productoBox.Visible)
                {
                    desdeLabel.Visible = false;
                    desdeBox.Visible = false;
                    hastaLabel.Visible = false;
                    hastaBox.Visible = false;
                }

                if (mainMenu.fn.MostrarPrecioAlterno)
                {

                    //totalBoxMultiMoneda.Location = new Point(75, 168);
                    //totalBoxMultiMoneda.Size = new Size(240, 34);
                    // Esto perjudicaria despues de haber llamado a AjustarFormularios
                    // Se dejaran estos valores en tiempo de diseño.

                    mDecimales = (mainMenu.fn.MonedaAlt_Dec == 0 ? String.Empty : "." + String.Empty.PadRight(mainMenu.fn.MonedaAlt_Dec, '0'));

                    totalBoxMultiMoneda.Text = MontoExacto(mainMenu.fn.PrecioFinalAlt, mainMenu.fn.MonedaAlt_Dec)
                    .ToString("#,###,###,###,##0" + mDecimales) + " " + mainMenu.fn.MonedaAlt_Sim;

                    if (totalBoxMultiMoneda.Text.Length >= 17)
                    {
                        totalBoxMultiMoneda.Font = new Font(totalBoxMultiMoneda.Font.Name,
                        (totalBoxMultiMoneda.Font.Size * 0.65f), totalBoxMultiMoneda.Font.Style);
                    }

                    totalBoxMultiMoneda.Visible = true;

                }

                if (mainMenu.fn.MostrarPrecioAlterno2)
                {

                    //totalBoxMultiMoneda.Location = new Point(163, 168);
                    //totalBoxMultiMoneda.Size = new Size(154, 34);
                    //totalBoxMultiMoneda2.Location = new Point(3, 168);
                    //totalBoxMultiMoneda2.Size = new Size(154, 34);
                    // Esto perjudicaria despues de haber llamado a AjustarFormularios
                    // Se dejaran estos valores en tiempo de diseño.

                    totalBoxMultiMoneda.Font = new Font("Arial", 16, FontStyle.Bold);
                    totalBoxMultiMoneda2.Font = new Font("Arial", 16, FontStyle.Bold);

                    mDecimales = (mainMenu.fn.MonedaAlt2_Dec == 0 ? String.Empty : "." + String.Empty.PadRight(mainMenu.fn.MonedaAlt2_Dec, '0'));

                    totalBoxMultiMoneda2.Text = MontoExacto(mainMenu.fn.PrecioFinalAlt2, mainMenu.fn.MonedaAlt2_Dec)
                    .ToString("#,###,###,###,##0" + mDecimales) + " " + mainMenu.fn.MonedaAlt2_Sim;

                    if (totalBoxMultiMoneda2.Text.Length >= 17)
                    {
                        totalBoxMultiMoneda2.Font = new Font(totalBoxMultiMoneda2.Font.Name, 
                        (totalBoxMultiMoneda2.Font.Size * 0.65f), totalBoxMultiMoneda2.Font.Style);
                    }

                    totalBoxMultiMoneda.Visible = true;
                    totalBoxMultiMoneda2.Visible = true;

                }

                //CtrlEvitarFocoTeclado.Focus();

            }
            catch (Exception e)
            {
                Variables test = new Variables();
                test.escribirLogError(e.Message, "detallesProducto");
                test = null;
            }

            //GC.Collect(); Movido a Rutina de Escaneo.

        }

        private void limpiar() 
        {
            descripcionBox.Text = String.Empty;
            precioBox.Text = String.Empty;
            ivaBox.Text = String.Empty;
            totalBox.Text = String.Empty;
            ofertaBox.Text = String.Empty;
            desdeBox.Text = String.Empty;
            hastaBox.Text = String.Empty;
            totalBoxMultiMoneda.Text = String.Empty;
            totalBoxMultiMoneda2.Text = String.Empty;
        }

        private void ocultarOferta()
        {
            ofertaLabel.Visible = false;
            ofertaBox.Visible = false;
            desdeLabel.Visible = false;
            desdeBox.Visible = false;
            hastaLabel.Visible = false;
            hastaBox.Visible = false;
        }

        private Double MontoExacto(Double Monto, Int32 Decimales)
        {

            if (!mainMenu.fn.Precios_ConsultaMonedaAlterna_RedondearHaciaArriba)
            {
                return Monto;
            }

            Double mValorMinimoAutoCompletar, mValorExacto, mMontoRound, mDiferencia;
            String MontoTxt, mDecFmt;

            mValorMinimoAutoCompletar = (1D / Math.Pow(10D, (double) Decimales));

            mValorExacto = Monto;

            mDecFmt = (Decimales == 0 ? String.Empty : "." + String.Empty.PadRight(Decimales, '0'));

            MontoTxt = mValorExacto.ToString("######0" + mDecFmt, System.Globalization.CultureInfo.InvariantCulture);

            mMontoRound = Convert.ToDouble(MontoTxt);

            mDiferencia = Math.Round(mValorExacto - mMontoRound, 8);

            if (mDiferencia > 0)
                return (mMontoRound + mValorMinimoAutoCompletar);
            else
                return mMontoRound;

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {

                //foreach (Control childControl in this.Controls)
                //{
                //    if (childControl.Focused)
                //    {
                //        //return childControl;
                //        //MessageBox.Show(this.CtrlEvitarFocoTeclado.Focused.ToString());
                //        MessageBox.Show(childControl.Name);
                //    }
                //}
                
                timer1.Enabled = false;
                this.Close();
                //GC.Collect(); Movido a Rutina de Escaneo
            }
            catch (Exception ee)
            {
                Console.WriteLine(ee);
            }
        }

        private void detallesProducto_Load(object sender, EventArgs e)
        {
            if (mainMenu.fn.TestMode)
            {
                timer1.Interval = 10000;
            }
        }

        private void detallesProducto_Activated(object sender, EventArgs e)
        {
            if (!FormaCargada)
            {
                FormaCargada = true;
                mainMenu.fn.AjustarFormularios(this);
                CtrlEvitarFoco.Focus(); // Esto para que no le quede el foco a los TextBox
                // y no salte el teclado virtual del dispositivo automaticamente
                // La solucion ideal seria cambiar todos los Text a Label... pero -_-
            }
        }

        private void hastaBox_ParentChanged(object sender, EventArgs e)
        {

        }

        private void inputPanel1_EnabledChanged(object sender, EventArgs e)
        {
            
        }

    }
}