﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using isPRICE;

namespace Stellar_isPRICE
{
    public partial class productoNoRegistrado : Form
    {
        public productoNoRegistrado()
        {
            InitializeComponent();
        }

        private void timerNoReg_Tick(object sender, EventArgs e)
        {
            timerNoReg.Enabled = false;
            this.Close();
        }

        private void productoNoRegistrado_Load(object sender, EventArgs e)
        {
            mainMenu.fn.AjustarFormularios(this);
        }
    }
}