﻿namespace Stellar_isPRICE
{
    partial class detallesProducto
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben eliminar; en caso contrario, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(detallesProducto));
            this.precioLabel = new System.Windows.Forms.Label();
            this.ivaLabel = new System.Windows.Forms.Label();
            this.totalLabel = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer();
            this.footerBox = new System.Windows.Forms.PictureBox();
            this.productoBox = new System.Windows.Forms.PictureBox();
            this.desdeLabel = new System.Windows.Forms.Label();
            this.hastaLabel = new System.Windows.Forms.Label();
            this.ofertaLabel = new System.Windows.Forms.Label();
            this.CtrlEvitarFoco = new System.Windows.Forms.CheckBox();
            this.descripcionBox = new System.Windows.Forms.Label();
            this.precioBox = new System.Windows.Forms.Label();
            this.ivaBox = new System.Windows.Forms.Label();
            this.ofertaBox = new System.Windows.Forms.Label();
            this.desdeBox = new System.Windows.Forms.Label();
            this.hastaBox = new System.Windows.Forms.Label();
            this.totalBox = new System.Windows.Forms.Label();
            this.totalBoxMultiMoneda = new System.Windows.Forms.Label();
            this.totalBoxMultiMoneda2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // precioLabel
            // 
            this.precioLabel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.precioLabel.Location = new System.Drawing.Point(14, 51);
            this.precioLabel.Name = "precioLabel";
            this.precioLabel.Size = new System.Drawing.Size(97, 23);
            this.precioLabel.Text = "Precio:";
            // 
            // ivaLabel
            // 
            this.ivaLabel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.ivaLabel.Location = new System.Drawing.Point(14, 77);
            this.ivaLabel.Name = "ivaLabel";
            this.ivaLabel.Size = new System.Drawing.Size(99, 23);
            this.ivaLabel.Text = "I.V.A.:";
            // 
            // totalLabel
            // 
            this.totalLabel.Font = new System.Drawing.Font("Arial", 16F, System.Drawing.FontStyle.Bold);
            this.totalLabel.Location = new System.Drawing.Point(10, 133);
            this.totalLabel.Name = "totalLabel";
            this.totalLabel.Size = new System.Drawing.Size(100, 32);
            this.totalLabel.Text = "Total:";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 5000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // footerBox
            // 
            this.footerBox.Image = ((System.Drawing.Image)(resources.GetObject("footerBox.Image")));
            this.footerBox.Location = new System.Drawing.Point(0, 203);
            this.footerBox.Name = "footerBox";
            this.footerBox.Size = new System.Drawing.Size(320, 37);
            this.footerBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // productoBox
            // 
            this.productoBox.Image = ((System.Drawing.Image)(resources.GetObject("productoBox.Image")));
            this.productoBox.Location = new System.Drawing.Point(216, 26);
            this.productoBox.Name = "productoBox";
            this.productoBox.Size = new System.Drawing.Size(101, 101);
            this.productoBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            // 
            // desdeLabel
            // 
            this.desdeLabel.BackColor = System.Drawing.Color.White;
            this.desdeLabel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.desdeLabel.Location = new System.Drawing.Point(216, 51);
            this.desdeLabel.Name = "desdeLabel";
            this.desdeLabel.Size = new System.Drawing.Size(90, 16);
            this.desdeLabel.Text = "Desde:";
            this.desdeLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // hastaLabel
            // 
            this.hastaLabel.BackColor = System.Drawing.Color.White;
            this.hastaLabel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.hastaLabel.Location = new System.Drawing.Point(216, 87);
            this.hastaLabel.Name = "hastaLabel";
            this.hastaLabel.Size = new System.Drawing.Size(90, 16);
            this.hastaLabel.Text = "Hasta:";
            this.hastaLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ofertaLabel
            // 
            this.ofertaLabel.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.ofertaLabel.Location = new System.Drawing.Point(14, 103);
            this.ofertaLabel.Name = "ofertaLabel";
            this.ofertaLabel.Size = new System.Drawing.Size(99, 23);
            this.ofertaLabel.Text = "Prc. Oferta:";
            // 
            // CtrlEvitarFoco
            // 
            this.CtrlEvitarFoco.AutoCheck = false;
            this.CtrlEvitarFoco.Location = new System.Drawing.Point(306, 3);
            this.CtrlEvitarFoco.Name = "CtrlEvitarFoco";
            this.CtrlEvitarFoco.Size = new System.Drawing.Size(1, 1);
            this.CtrlEvitarFoco.TabIndex = 0;
            // 
            // descripcionBox
            // 
            this.descripcionBox.BackColor = System.Drawing.Color.White;
            this.descripcionBox.Font = new System.Drawing.Font("Arial", 12.5F, System.Drawing.FontStyle.Bold);
            this.descripcionBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.descripcionBox.Location = new System.Drawing.Point(14, 0);
            this.descripcionBox.Name = "descripcionBox";
            this.descripcionBox.Size = new System.Drawing.Size(303, 52);
            this.descripcionBox.Text = "DESCRIPCION DEL PRODUCTO";
            // 
            // precioBox
            // 
            this.precioBox.BackColor = System.Drawing.Color.White;
            this.precioBox.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.precioBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.precioBox.Location = new System.Drawing.Point(60, 51);
            this.precioBox.Name = "precioBox";
            this.precioBox.Size = new System.Drawing.Size(150, 23);
            this.precioBox.Text = "0.00";
            this.precioBox.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ivaBox
            // 
            this.ivaBox.BackColor = System.Drawing.Color.White;
            this.ivaBox.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.ivaBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ivaBox.Location = new System.Drawing.Point(55, 77);
            this.ivaBox.Name = "ivaBox";
            this.ivaBox.Size = new System.Drawing.Size(155, 23);
            this.ivaBox.Text = "0.00";
            this.ivaBox.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // ofertaBox
            // 
            this.ofertaBox.BackColor = System.Drawing.Color.White;
            this.ofertaBox.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.ofertaBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.ofertaBox.Location = new System.Drawing.Point(85, 103);
            this.ofertaBox.Name = "ofertaBox";
            this.ofertaBox.Size = new System.Drawing.Size(125, 23);
            this.ofertaBox.Text = "0.00";
            this.ofertaBox.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // desdeBox
            // 
            this.desdeBox.BackColor = System.Drawing.Color.White;
            this.desdeBox.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.desdeBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.desdeBox.Location = new System.Drawing.Point(216, 67);
            this.desdeBox.Name = "desdeBox";
            this.desdeBox.Size = new System.Drawing.Size(90, 16);
            this.desdeBox.Text = "01/01/1990";
            this.desdeBox.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // hastaBox
            // 
            this.hastaBox.BackColor = System.Drawing.Color.White;
            this.hastaBox.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Regular);
            this.hastaBox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.hastaBox.Location = new System.Drawing.Point(216, 103);
            this.hastaBox.Name = "hastaBox";
            this.hastaBox.Size = new System.Drawing.Size(90, 16);
            this.hastaBox.Text = "12/31/9999";
            this.hastaBox.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // totalBox
            // 
            this.totalBox.BackColor = System.Drawing.Color.White;
            this.totalBox.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.totalBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(181)))), ((int)(((byte)(27)))), ((int)(((byte)(18)))));
            this.totalBox.Location = new System.Drawing.Point(75, 133);
            this.totalBox.Name = "totalBox";
            this.totalBox.Size = new System.Drawing.Size(240, 34);
            this.totalBox.Text = "0.00";
            this.totalBox.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // totalBoxMultiMoneda
            // 
            this.totalBoxMultiMoneda.BackColor = System.Drawing.Color.White;
            this.totalBoxMultiMoneda.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.totalBoxMultiMoneda.ForeColor = System.Drawing.Color.Green;
            this.totalBoxMultiMoneda.Location = new System.Drawing.Point(163, 168);
            this.totalBoxMultiMoneda.Name = "totalBoxMultiMoneda";
            this.totalBoxMultiMoneda.Size = new System.Drawing.Size(154, 34);
            this.totalBoxMultiMoneda.Text = "0.00";
            this.totalBoxMultiMoneda.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.totalBoxMultiMoneda.Visible = false;
            // 
            // totalBoxMultiMoneda2
            // 
            this.totalBoxMultiMoneda2.BackColor = System.Drawing.Color.White;
            this.totalBoxMultiMoneda2.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold);
            this.totalBoxMultiMoneda2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.totalBoxMultiMoneda2.Location = new System.Drawing.Point(3, 168);
            this.totalBoxMultiMoneda2.Name = "totalBoxMultiMoneda2";
            this.totalBoxMultiMoneda2.Size = new System.Drawing.Size(154, 34);
            this.totalBoxMultiMoneda2.Text = "0.00";
            this.totalBoxMultiMoneda2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.totalBoxMultiMoneda2.Visible = false;
            // 
            // detallesProducto
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(320, 240);
            this.ControlBox = false;
            this.Controls.Add(this.totalBoxMultiMoneda2);
            this.Controls.Add(this.totalBoxMultiMoneda);
            this.Controls.Add(this.totalBox);
            this.Controls.Add(this.hastaBox);
            this.Controls.Add(this.desdeBox);
            this.Controls.Add(this.ofertaBox);
            this.Controls.Add(this.ivaBox);
            this.Controls.Add(this.precioBox);
            this.Controls.Add(this.descripcionBox);
            this.Controls.Add(this.CtrlEvitarFoco);
            this.Controls.Add(this.ofertaLabel);
            this.Controls.Add(this.desdeLabel);
            this.Controls.Add(this.hastaLabel);
            this.Controls.Add(this.productoBox);
            this.Controls.Add(this.footerBox);
            this.Controls.Add(this.ivaLabel);
            this.Controls.Add(this.totalLabel);
            this.Controls.Add(this.precioLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "detallesProducto";
            this.Text = "Stellar isPRICE - www.bigwise.com";
            this.Load += new System.EventHandler(this.detallesProducto_Load);
            this.Activated += new System.EventHandler(this.detallesProducto_Activated);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label precioLabel;
        private System.Windows.Forms.Label ivaLabel;
        private System.Windows.Forms.Label totalLabel;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox footerBox;
        private System.Windows.Forms.PictureBox productoBox;
        private System.Windows.Forms.Label desdeLabel;
        private System.Windows.Forms.Label hastaLabel;
        private System.Windows.Forms.Label ofertaLabel;
        private System.Windows.Forms.CheckBox CtrlEvitarFoco;
        private System.Windows.Forms.Label descripcionBox;
        private System.Windows.Forms.Label precioBox;
        private System.Windows.Forms.Label ivaBox;
        private System.Windows.Forms.Label ofertaBox;
        private System.Windows.Forms.Label desdeBox;
        private System.Windows.Forms.Label hastaBox;
        private System.Windows.Forms.Label totalBox;
        private System.Windows.Forms.Label totalBoxMultiMoneda;
        private System.Windows.Forms.Label totalBoxMultiMoneda2;
    }
}