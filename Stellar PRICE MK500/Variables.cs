﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace Stellar_isPRICE
{

    class Variables
    {

        private static String descri;
        private static Double impue;
        private static Double precioOfe;
        private static Double totalOfe;
        private static Double subTotal;
        private static Double prec;
        private static DateTime fechaIni;
        private static DateTime fechaLimi;
        private static Boolean tieneOf;
        private static String SimboloMoneda;
        private static String TaxIDLabel;
        private static String PriceLabel;
        private static String PriceOfferLabel;
        private static String TotalLabel;
        private static String FromLabel;
        private static String UntilLabel;
        private static Boolean MostrarProductImgNotFound;
        
        private static String rutaTxt = @"\Application\RemCapture\My Documents\isPRICE\LogError.txt";

        #region Gets and Sets
        public String Descri
        {
            get
            {
                return descri;
            }
            set
            {
                descri = value;
            }
        }

        public Double Impue
        {
            get
            {
                return impue;
            }
            set
            {
                impue = value;
            }
        }

        public Double PrecioOfe
        {
            get
            {
                return precioOfe;
            }
            set
            {
                precioOfe = value;
            }
        }

        public Double TotalOfe
        {
            get
            {
                return totalOfe;
            }
            set
            {
                totalOfe = value;
            }
        }

        public Double Subtotal
        {
            get
            {
                return subTotal;
            }
            set
            {
                subTotal = value;
            }
        }

        public Double Prec
        {
            get
            {
                return prec;
            }
            set
            {
                prec = value;
            }
        }

        public DateTime FechaIni
        {
            get
            {
                return fechaIni;
            }
            set
            {
                fechaIni = value;
            }
        }

        public DateTime FechaLimi
        {
            get
            {
                return fechaLimi;
            }
            set
            {
                fechaLimi = value;
            }
        }

        public Boolean TieneOf
        {
            get
            {
                return tieneOf;
            }
            set
            {
                tieneOf = value;
            }
        }

        public Boolean VarMostrarProductImgNotFound
        {
            get
            {
                return MostrarProductImgNotFound;
            }
            set
            {
                MostrarProductImgNotFound = value;
            }
        }

        public String RutaTxt
        {
            get
            {
                return rutaTxt;
            }
            set
            {
                rutaTxt = value;
            }
        }

        public String SimMoneda
        {
            get
            {
                return SimboloMoneda;
            }
            set
            {
                SimboloMoneda = value;
            }
        }

        public String LabelTaxID
        {
            get
            {
                return TaxIDLabel;
            }
            set
            {
                TaxIDLabel = value;
            }
        }

        public String LabelPrice
        {
            get
            {
                return PriceLabel;
            }
            set
            {
                PriceLabel = value;
            }
        }

        public String LabelPriceOffer
        {
            get
            {
                return PriceOfferLabel;
            }
            set
            {
                PriceOfferLabel = value;
            }
        }

        public String LabelTotal
        {
            get
            {
                return TotalLabel;
            }
            set
            {
                TotalLabel = value;
            }
        }

        public String LabelFrom
        {
            get
            {
                return FromLabel;
            }
            set
            {
                FromLabel = value;
            }
        }

        public String LabelUntil
        {
            get
            {
                return UntilLabel;
            }
            set
            {
                UntilLabel = value;
            }
        }

        #endregion

        public void receiveData(String desc, Double imp, Double precioOf, Double totalOf, 
        Double subt, Double pre, DateTime fInicio, DateTime fLimite, Boolean oferta)
        {
            descri = desc;
            impue = imp;
            precioOfe = precioOf;
            totalOfe = totalOf;
            subTotal = subt;
            prec = pre;
            fechaIni = fInicio;
            fechaLimi = fLimite;
            tieneOf = oferta;
        }

        public void escribirLogError(String excepcion, String metodo)
        {
            try
            {
                System.IO.StreamWriter sw = new System.IO.StreamWriter(rutaTxt, true);
                sw.WriteLine(DateTime.Now.ToString() + " Metodo: " + metodo + "Excepcion: " + excepcion);
                sw.Close();
            }
            catch (Exception Any)
            {
                Console.WriteLine(Any.Message);
            }
        }

    }

}
