﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Symbol.Exceptions;
using Symbol.Barcode2;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using System.Data.SqlClient;
using System.Xml;
using isPRICE;
using Stellar_isPRICE;
using Microsoft.VisualBasic;

namespace isPRICE
{

    public partial class mainMenu : Form
    {

        public static Funciones fn = null;

        //Scanner
        private API scanAPI = null;
        private Barcode2.OnScanHandler scanHandler = null;
        private Barcode2.OnStatusHandler statusHandler = null;
        private EventHandler myFormActivatedEventHandler = null;
        private EventHandler myFormDeactivatedEventHandler = null;
        private bool barcodeActivated = false;
        public int ImgCounter = 0;

        //Setup
        XmlTextReader xmlReader = new XmlTextReader(@"My Documents\isPRICE\setup.xml");
        //static string BD = "VAD20";
        //static string Server = @"192.168.28.194\CLIENTE06";
        static String BD;
        static String Server;
        static String Login;
        static String Pwd;
        static String SimboloMoneda = null;
        static String RootImg = null;
        static Boolean FactorizarPrecios = false;
        static Boolean MostrarProductImgNotFound = false;
        static Boolean PermitirConsultaPrecioMatrizCodigo = true;
        static Boolean PostConsultaEtiquetaPrecioUnitario = true;
        static Boolean MaestroEtiquetaPrecioIncluyeIva = false;
        static String LabelTaxID = null;
        static String LabelPrice = null;
        static String LabelPriceOffer = null;
        static String LabelTotal = null;
        static String LabelFrom = null;
        static String LabelUntil = null;

        static int ImgCycleMSInterval = 0;
        //detallesProducto detalles = new detallesProducto();

        //SQL
        static String url = null;
        static SqlConnection connection;
        static SqlDataAdapter da_codigo;
        static SqlDataAdapter da_serverDate;
        static DataTable dt_codigo;
        static DataTable dt_serverDate;

        //Variables
        private static String codnasa;
        private static String descripcion;
        private static Double tasa;
        private static Double impuesto;
        private static Double precio;
        private static Double precioOferta;
        private static Double totalOferta;
        private static Double PrecioTotalPred;
        private static Double PrecioTotalOfertaPred;
        private static Double subtotal;
        static DateTime serverDateTime;
        static DateTime fInicio;
        static DateTime fFin;
        static DateTime hInicio;
        static DateTime hFin;
        static String hSimbolo;
        static String hCodMon;
        static String hDesMon;
        static Double hFactor;
        private static DateTime fechaInicio;
        private static DateTime fechaLimite;
        private Boolean tieneOferta;
        public static mainMenu menu = new mainMenu();
        Variables var = new Variables();

        #region Get and Sets
        public String Codnasa
        {
            get
            {
                return codnasa;
            }
            set
            {
                codnasa = value;
            }
        }

        public String Descripcion
        {
            get
            {
                return descripcion;
            }
            set
            {
                descripcion = value;
            }
        }

        public Double Impuesto
        {
            get
            {
                return impuesto;
            }
            set
            {
                impuesto = value;
            }
        }

        public Double Precio
        {
            get
            {
                return precio;
            }
            set
            {
                precio = value;
            }
        }

        public Double Subtotal
        {
            get
            {
                return subtotal;
            }
            set
            {
                subtotal = value;
            }
        }

        public Double PrecioOferta
        {
            get
            {
                return precioOferta;
            }
            set
            {
                precioOferta = value;
            }
        }

        public Double TotalOferta
        {
            get
            {
                return totalOferta;
            }
            set
            {
                totalOferta = value;
            }
        }

        public DateTime FechaInicio
        {
            get
            {
                return fechaInicio;
            }
            set
            {
                fechaInicio = value;
            }
        }

        public DateTime FechaLimite
        {
            get
            {
                return fechaLimite;
            }
            set
            {
                fechaLimite = value;
            }
        }

        public Boolean TieneOferta
        {
            get
            {
                return tieneOferta;
            }
            set
            {
                tieneOferta = value;
            }
        }
        #endregion

        public mainMenu()
        {
            //19695633
            try
            {
                InitializeComponent();
            }
            catch (Exception e)
            {
                var.escribirLogError(e.Message, "public mainMenu()");
            }
            finally
            {
                GC.Collect();
            }
        }

        static void Main()
        {
            try
            {
                fn = new Funciones();
                Application.Run(menu);
            }
            catch (Exception e)
            {
                Variables v = new Variables();
                v.escribirLogError(e.Message, "static void Main()");
                v = null;
            }
        }

        private void readConfig()
        {

            String mValFactorizarPrecios = null;
            String mValDebugMode = null;
            String mValTestMode = null;
            String mValMostrarProductImgNotFound = null;
            String mPermitirConsultaPrecioMatrizCodigo = null;
            String mPostConsultaEtiquetaPrecioUnitario = null;
            String mMaestroEtiquetaPrecioIncluyeIva = null;
            String mValPrecios_ConsultaMonedaAlterna_RedondearHaciaArriba = null;
            String mValPrecios_ConsultaMonedaAlterna_DecimalesPantalla = null;
            String mValPrecios_ConsultaMonedaAlterna2_DecimalesPantalla = null;
            String mValImpresoraFiscal_DecimalesPrecio = null;
            String mValTipoPrecioMostrar = null;
            String mValNombre_BD_ADM = null; // Por Defecto VAD10
            String mValNombre_BD_POS = null; // Por Defecto VAD20

            fn.FormDesignHeight = "240";
            fn.FormDesignWidth = "320";
            fn.DebugMode = false;
            fn.TestMode = false;

            try
            {
                while (xmlReader.Read())
                {

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("baseDatos")))
                    {
                        BD = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("server")))
                    {
                        Server = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("Login")))
                    {
                        Login = xmlReader.ReadString();
                    }

                    if (String.IsNullOrEmpty(Login))
                    {
                        Login = "SA";
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                        && (xmlReader.Name.Equals("Pwd")))
                    {
                        Pwd = xmlReader.ReadString();
                    }

                    if (String.IsNullOrEmpty(Pwd))
                    {
                        Pwd = String.Empty;
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("SimboloMoneda", StringComparison.OrdinalIgnoreCase)))
                    {
                        SimboloMoneda = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("LabelTaxID", StringComparison.OrdinalIgnoreCase)))
                    {
                        LabelTaxID = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("LabelPrice", StringComparison.OrdinalIgnoreCase)))
                    {
                        LabelPrice = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("LabelPriceOffer", StringComparison.OrdinalIgnoreCase)))
                    {
                        LabelPriceOffer = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("LabelTotal", StringComparison.OrdinalIgnoreCase)))
                    {
                        LabelTotal = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("LabelFrom", StringComparison.OrdinalIgnoreCase)))
                    {
                        LabelFrom = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("LabelUntil", StringComparison.OrdinalIgnoreCase)))
                    {
                        LabelUntil = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                        && (xmlReader.Name.Equals("ImgRootDir", StringComparison.OrdinalIgnoreCase)))
                    {
                        RootImg = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("FactorizarPrecios", StringComparison.OrdinalIgnoreCase)))
                    {
                        mValFactorizarPrecios = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("MostrarProductImgNotFound", StringComparison.OrdinalIgnoreCase)))
                    {
                        mValMostrarProductImgNotFound = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                        && (xmlReader.Name.Equals("Intervalo_Img_MS", StringComparison.OrdinalIgnoreCase)))
                    {
                        ImgCycleMSInterval = int.Parse(xmlReader.ReadString());
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                        && (xmlReader.Name.Equals("Screen_FormDesignHeight", StringComparison.OrdinalIgnoreCase)))
                    {
                        fn.FormDesignHeight = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                        && (xmlReader.Name.Equals("Screen_FormDesignWidth", StringComparison.OrdinalIgnoreCase)))
                    {
                        fn.FormDesignWidth = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                        && (xmlReader.Name.Equals("Screen_CurrentHeight", StringComparison.OrdinalIgnoreCase)))
                    {
                        fn.CurrentScreenHeight = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                        && (xmlReader.Name.Equals("Screen_CurrentWidth", StringComparison.OrdinalIgnoreCase)))
                    {
                        fn.CurrentScreenWidth = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("DebugMode", StringComparison.OrdinalIgnoreCase)))
                    {
                        mValDebugMode = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("TestMode", StringComparison.OrdinalIgnoreCase)))
                    {
                        mValTestMode = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("Precios_ConsultaMonedaAlterna", StringComparison.OrdinalIgnoreCase)))
                    {
                        fn.Precios_ConsultaMonedaAlterna = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("Precios_ConsultaMonedaAlterna2", StringComparison.OrdinalIgnoreCase)))
                    {
                        fn.Precios_ConsultaMonedaAlterna2 = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("Precios_ConsultaMonedaAlterna_DecimalesPantalla", StringComparison.OrdinalIgnoreCase)))
                    {
                        mValPrecios_ConsultaMonedaAlterna_DecimalesPantalla = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("Precios_ConsultaMonedaAlterna2_DecimalesPantalla", StringComparison.OrdinalIgnoreCase)))
                    {
                        mValPrecios_ConsultaMonedaAlterna2_DecimalesPantalla = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("Precios_ConsultaMonedaAlterna_RedondearHaciaArriba", StringComparison.OrdinalIgnoreCase)))
                    {
                        mValPrecios_ConsultaMonedaAlterna_RedondearHaciaArriba = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("ImpresoraFiscal_DecimalesPrecio", StringComparison.OrdinalIgnoreCase)))
                    {
                        mValImpresoraFiscal_DecimalesPrecio = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("PermitirConsultaPrecioMatrizCodigo", StringComparison.OrdinalIgnoreCase)))
                    {
                        mPermitirConsultaPrecioMatrizCodigo = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("PostConsultaEtiquetaPrecioUnitario", StringComparison.OrdinalIgnoreCase)))
                    {
                        mPostConsultaEtiquetaPrecioUnitario = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("MaestroEtiquetaPrecioIncluyeIva", StringComparison.OrdinalIgnoreCase)))
                    {
                        mMaestroEtiquetaPrecioIncluyeIva = xmlReader.ReadString();
                    }
                    
                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("TipoPrecioMostrar", StringComparison.OrdinalIgnoreCase)))
                    {
                        mValTipoPrecioMostrar = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("BD_ADM", StringComparison.OrdinalIgnoreCase)))
                    {
                        mValNombre_BD_ADM = xmlReader.ReadString();
                    }

                    if ((xmlReader.NodeType.Equals(XmlNodeType.Element))
                    && (xmlReader.Name.Equals("BD_POS", StringComparison.OrdinalIgnoreCase)))
                    {
                        mValNombre_BD_POS = xmlReader.ReadString();
                    }
                }

            }
            catch (System.IO.FileNotFoundException fnfe)
            {
                MessageBox.Show(fnfe.Message);
                Application.Exit();
            }

            if ((SimboloMoneda == null))
            {
                SimboloMoneda = "Bs.";
            }

            if ((LabelTaxID == null))
            {
                LabelTaxID = "I.V.A";
            }

            if ((LabelPrice == null))
            {
                LabelPrice = "Precio";
            }

            if ((LabelPriceOffer == null))
            {
                LabelPriceOffer = "Prc. Oferta";
            }

            if ((LabelTotal == null))
            {
                LabelTotal = "Total";
            }

            if ((LabelFrom == null))
            {
                LabelFrom = "Desde";
            }

            if ((LabelUntil == null))
            {
                LabelUntil = "Hasta";
            }

            if ((RootImg == null))
            {
                RootImg = "Img";
            }

            if (String.IsNullOrEmpty(mValFactorizarPrecios))
            {
                FactorizarPrecios = true;
            }
            else
            {
                if (mValFactorizarPrecios == "0")
                    FactorizarPrecios = false;
                else
                    FactorizarPrecios = true;
            }

            if (String.IsNullOrEmpty(mValPrecios_ConsultaMonedaAlterna_RedondearHaciaArriba))
            {
                fn.Precios_ConsultaMonedaAlterna_RedondearHaciaArriba = false;
            }
            else
            {
                if (mValFactorizarPrecios == "0")
                    fn.Precios_ConsultaMonedaAlterna_RedondearHaciaArriba = false;
                else
                    fn.Precios_ConsultaMonedaAlterna_RedondearHaciaArriba = true;
            }

            if (String.IsNullOrEmpty(mValPrecios_ConsultaMonedaAlterna_DecimalesPantalla))
            {
                fn.Precios_ConsultaMonedaAlterna_DecimalesPantalla = -1;
            }
            else
            {
                fn.Precios_ConsultaMonedaAlterna_DecimalesPantalla = Int32.Parse(mValPrecios_ConsultaMonedaAlterna_DecimalesPantalla);
            }

            if (String.IsNullOrEmpty(mValPrecios_ConsultaMonedaAlterna2_DecimalesPantalla))
            {
                fn.Precios_ConsultaMonedaAlterna2_DecimalesPantalla = -1;
            }
            else
            {
                fn.Precios_ConsultaMonedaAlterna2_DecimalesPantalla = Int32.Parse(mValPrecios_ConsultaMonedaAlterna2_DecimalesPantalla);
            }

            if (String.IsNullOrEmpty(mValMostrarProductImgNotFound))
            {
                MostrarProductImgNotFound = false;
            }
            else
            {
                if (mValMostrarProductImgNotFound == "1")
                    MostrarProductImgNotFound = true;
            }

            if (ImgCycleMSInterval == 0)
            {
                ImgCycleMSInterval = 10000;
                ImgTimer.Interval = ImgCycleMSInterval;
            }
            else if (ImgCycleMSInterval == -1)
            {
                ImgTimer.Enabled = false;
                ImgTimer.Interval = 0;
            }

            if (mValDebugMode == "1")
            {
                fn.DebugMode = true;
            }

            if (mValTestMode == "1")
            {
                fn.TestMode = true;
            }

            if (String.IsNullOrEmpty(mPermitirConsultaPrecioMatrizCodigo))
            {
                PermitirConsultaPrecioMatrizCodigo = true;
            }
            else
            {
                if (mPermitirConsultaPrecioMatrizCodigo == "0")
                    PermitirConsultaPrecioMatrizCodigo = false;
                else
                    PermitirConsultaPrecioMatrizCodigo = true;
            }

            if (String.IsNullOrEmpty(mPostConsultaEtiquetaPrecioUnitario))
            {
                PostConsultaEtiquetaPrecioUnitario = true;
            }
            else
            {
                if (mPostConsultaEtiquetaPrecioUnitario == "0")
                    PostConsultaEtiquetaPrecioUnitario = false;
                else
                    PostConsultaEtiquetaPrecioUnitario = true;
            }

            if (String.IsNullOrEmpty(mMaestroEtiquetaPrecioIncluyeIva))
            {
                MaestroEtiquetaPrecioIncluyeIva = false;
            }
            else
            {
                if (mMaestroEtiquetaPrecioIncluyeIva == "1")
                    MaestroEtiquetaPrecioIncluyeIva = true;
                else
                    MaestroEtiquetaPrecioIncluyeIva = false;
            }

            if (String.IsNullOrEmpty(fn.Precios_ConsultaMonedaAlterna) 
            && !String.IsNullOrEmpty(fn.Precios_ConsultaMonedaAlterna2))
            {
                // No se puede especificar moneda alterna 2 sin especificar la primera.
                fn.Precios_ConsultaMonedaAlterna = String.Empty;
                fn.Precios_ConsultaMonedaAlterna2 = String.Empty;
            }
            
            if (String.IsNullOrEmpty(mValImpresoraFiscal_DecimalesPrecio))
            {
                fn.ImpresoraFiscal_DecimalesPrecio = -1;
            }
            else
            {
                fn.ImpresoraFiscal_DecimalesPrecio = Int32.Parse(mValImpresoraFiscal_DecimalesPrecio);
            }

            if (String.IsNullOrEmpty(mValTipoPrecioMostrar))
            {
                fn.TipoPrecioMostrar = "0";
            }
            else
            {
                fn.TipoPrecioMostrar = mValTipoPrecioMostrar;
            }

            if(String.IsNullOrEmpty(mValNombre_BD_ADM)) 
            {
                mValNombre_BD_ADM = "VAD10";
            }
            fn.BD_ADM = mValNombre_BD_ADM;

            if (String.IsNullOrEmpty(mValNombre_BD_POS))
            {
                mValNombre_BD_POS = "VAD20";
            }
            fn.BD_POS = mValNombre_BD_POS;

            url = "Data Source=" + Server + ";" +
            "Initial Catalog=" + BD + ";" +
            "User ID=" + Login + ";" +
            (!String.IsNullOrEmpty(Pwd) ?
            "Pwd=" + Pwd + ";" : String.Empty) +
            "Connection Timeout=20";

            // ReadConfig END

            if (!(Information.IsNumeric(fn.FormDesignHeight) 
            && Information.IsNumeric(fn.FormDesignWidth) 
            && Information.IsNumeric(fn.CurrentScreenHeight) 
            && Information.IsNumeric(fn.CurrentScreenWidth)))
            {
                fn.RuntimeVSDesignRatioX = 1;
                fn.RuntimeVSDesignRatioY = 1;
            } else {
                fn.RuntimeVSDesignRatioX = (Conversion.Val(fn.FormDesignWidth) / 240); // 240 = Px de Ancho en los que esta hecho el PRICE.
                fn.RuntimeVSDesignRatioY = (Conversion.Val(fn.FormDesignHeight) / 320);// 320 = Px de Altura en los que esta hecho el PRICE.
            }

        }

        private void ChangeIMG()
        {

            try
            {

                String MyPath = System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase.Substring(0,
                (System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase.Length -
                (System.Reflection.Assembly.GetExecutingAssembly().GetName().Name.Length +
                ".exe".Length)));

                String ImgRoot = RootImg;

                var ext = new List<String> { "jpg", "png" };

                String[] filePaths;// = Directory.GetFiles(MyPath, "*.png");

                if (String.IsNullOrEmpty(ImgRoot.Trim()))
                {
                    filePaths = Directory.GetFiles(MyPath, "*.png");
                }
                else
                {
                    filePaths = Directory.GetFiles(MyPath + ImgRoot, "*.png");
                }

                if (filePaths.Length <= 0) return;

                flechaTimer.Enabled = false;
                codigoBox.Visible = false;

                if (!flechaBox.Visible)
                    flechaTimer_Tick(null, null);

                ImgCounter++;

                if (ImgCounter == filePaths.Length)
                {
                    ImgCounter = 0;
                }

                wallpaperBox.SizeMode = PictureBoxSizeMode.StretchImage;
                this.wallpaperBox.Image = new Bitmap(filePaths.GetValue(ImgCounter).ToString());
                flechaBox.SizeMode = PictureBoxSizeMode.StretchImage;
                this.flechaBox.Image = this.wallpaperBox.Image;

            }
            catch (Exception ex)
            {
                Variables v = new Variables();
                v.escribirLogError(ex.Message, "ChangeIMG():");
                v = null;
            }
            finally
            {
                GC.Collect();
            }
        }

        private void mainMenu_Load(object sender, EventArgs e)
        {

            //Lee las configuraciones del setup.xml
            readConfig();

            //Inicia el objeto que maneja las clases del scanner.
            this.scanAPI = new API();

            if (fn.TestMode)
            {
                codigoBox.Visible = true;
                codigoBox.Enabled = true;
                codigoBox.ReadOnly = false;
                codigoBox.Focus();
            }
            else
            {
                //Recibe como dato el status del barcode.
                this.barcodeActivated = this.scanAPI.InitBarcode();

                if (!(this.barcodeActivated))
                {
                    MessageBox.Show("No se pudo inicializar el barcode");
                    Application.Exit();
                }
                else
                {
                    //statusBar.Text = "Listo";

                    // Attach a scan notification handler.
                    this.scanHandler = new Barcode2.OnScanHandler(myBarcode2_ScanNotify);
                    scanAPI.AttachScanNotify(scanHandler);

                    // Attach a status notification handler.
                    this.statusHandler = new Barcode2.OnStatusHandler(myBarcode2_StatusNotify);
                    scanAPI.AttachStatusNotify(statusHandler);
                }

                myFormActivatedEventHandler = new EventHandler(mainMenu_Activated);
                myFormDeactivatedEventHandler = new EventHandler(mainMenu_Deactivate);
                this.Activated += myFormActivatedEventHandler;
                this.Deactivate += myFormDeactivatedEventHandler;
            }

            fn.AjustarFormularios(this);

        }

        /// <summary>
        /// Read notification handler.
        /// </summary>
        private void myBarcode2_ScanNotify(ScanDataCollection scanDataCollection)
        {

            try
            {

                if (this.InvokeRequired)
                {
                    this.BeginInvoke(new Barcode2.OnScanHandler(myBarcode2_ScanNotify), 
                    new object[] { scanDataCollection });
                }
                else
                {
                    //Obtiene el dato escaneado
                    ScanData scanData = scanDataCollection.GetFirst;

                    switch (scanData.Result)
                    {
                        case Results.SUCCESS:
                            HandleContinuousData(scanData);
                            this.scanAPI.StartScan(true);
                            break;

                        case Results.E_SCN_READTIMEOUT:
                            this.scanAPI.StartScan(true);
                            break;

                        case Results.CANCELED:
                            break;

                        case Results.E_SCN_DEVICEFAILURE:
                            this.scanAPI.StopScan();
                            this.scanAPI.StartScan(true);
                            break;

                        default:
                            string sMsg = "Read Failed\n"
                                + "Result = "
                                + (scanData.Result).ToString();
                            
                            var.escribirLogError(sMsg, "myBarcode2_ScanNotify");

                            if (scanData.Result == Results.E_SCN_READINCOMPATIBLE)
                            {
                                var.escribirLogError("Escaner Incompatible. La aplicacion va a terminar", "myBarcode2_ScanNotify");
                                MessageBox.Show("Escaner Incompatible. La aplicacion va a terminar");
                                this.Close();
                                return;
                            }
                            break;
                    }
                }

            }
            catch (Exception Any)
            {
                var.escribirLogError(Any.Message, "Barcode_ScanNotify(ScanDataCollection)");
            }
            finally
            {
                GC.Collect();
            }

        }

        /// <summary>
        /// Status notification handler.
        /// </summary>
        private void myBarcode2_StatusNotify(StatusData statusData)
        {

            try
            {

                if (this.InvokeRequired)
                {
                    this.Invoke(new Barcode2.OnStatusHandler(myBarcode2_StatusNotify), new object[] 
                {
                    statusData 
                });
                }
                else
                {
                    switch (statusData.State)
                    {
                        case States.IDLE:
                            //statusBar.Text = "Coloque el producto en el lector";
                            break;

                        case States.READY:
                            //statusBar.Text = "Coloque el producto en el lector";
                            break;

                        default:
                            //statusBar.Text = statusData.Text;
                            break;
                    }
                }

            }
            catch (Exception Any)
            {
                Console.WriteLine(Any.Message);
            }
            
        }

        private void Consultar(String scanData)
        {

            String Codigo = String.Empty;
            String mJoinMoneda = String.Empty;
            String mMultiplicadorCampos = String.Empty;
            String mFactorizarQuery = String.Empty;
            String consultaCodigo = String.Empty;
            fn.MostrarPrecioAlterno = false;

            Boolean EtiquetaProd = false;

            Boolean Cal_Precio = false;
            Double BsBal = 0;
            Double DecBal = 0;
            Double PrecioBal = 0;
            Double PesoBal = 0;
            Double PesoBalDec = 0;
            Double PrecioVenta = 0;
            Double PrecioReal = 0;
            Double PrecioOferta = 0;

            Double CantEtiq = 0, PrecioEtiq = 0, SubtotalEtiq = 0;

            try
            {

                codigoBox.Text = scanData;
                connection = new SqlConnection(url);

                Codigo = scanData;

                connection.Open();

                /*string consultaCodigo = "SELECT C_CODIGO, C_DESCRI, n_impuesto1, ROUND(n_precio1, 2) AS precio1, ROUND(n_precioO, 2) AS precioOf, ROUND(n_precio1 * (n_impuesto1 / 100), 2) AS IVA, "
                + " CASE WHEN n_impuesto1 = '0' THEN ROUND(n_precio1, 2) ELSE ROUND(n_precio1 + n_precio1 * (n_impuesto1 / 100), 2) END AS TotalGral, "
                + " CASE WHEN n_impuesto1 = '0' THEN ROUND(n_precioO, 2) ELSE ROUND(n_precioO + n_precioO * (n_impuesto1 / 100), 2) END AS TotalOf, "
                + " f_inicial, f_final, h_inicial, h_final FROM MA_PRODUCTOS WHERE C_CODIGO IN (SELECT c_codnasa FROM MA_CODIGOS WHERE (c_codigo = '" + codigo + "'))";*/

                if (PermitirConsultaPrecioMatrizCodigo)
                {
                    
                    String Text1 = Codigo;

                    String mSQLEtiquetas = "SELECT * FROM " + fn.BD_POS + ".DBO.MA_ETIQUETAS WHERE \n" +
                    "c_Longitud = '" + fn.FixTSQL(Text1).Trim().Substring(0, 2) + "' \n" +
                    "AND b_Activa = 1 \n" +
                    "AND n_Longitud = " + Text1.Length + " \n";

                    SqlDataAdapter daMatriz = new SqlDataAdapter(mSQLEtiquetas, connection);
                    DataTable dtMatriz = new DataTable(); ;
                    Int32 ResMatriz = daMatriz.Fill(dtMatriz);

                    if (ResMatriz > 0 && dtMatriz.Rows.Count > 0)
                    {

                        Boolean mVerificaDigito = true;

                        DataRow row = dtMatriz.Rows[0];

                        if (Convert.ToBoolean(row["b_VerificaDigito"]))
                            if (!fn.VerificarDigito(Text1))
                                mVerificaDigito = false;
                        
                        if (mVerificaDigito)
                        {

                            switch (Convert.ToInt32(row["c_Campo"]))
                            {
                                
                                case 0: //**** PRECIO
                                    
                                    Cal_Precio = true;
                                    
                                    Text1 = Text1.Substring(Convert.ToInt32(row["n_Pos_Ini_1"]) - 1, Convert.ToInt32(row["n_Caracteres_1"]));
                                    
                                    EtiquetaProd = true;
                                    
                                    if ((Convert.ToInt32(row["n_Pos_Ini_3"]) - 1) != 0)
                                    {
                                        BsBal = Convert.ToDouble(Codigo.Substring(Convert.ToInt32(row["n_Pos_Ini_2"]) - 1, Convert.ToInt32(row["n_Caracteres_2"])));
                                        DecBal = Convert.ToDouble(Codigo.Substring(Convert.ToInt32(row["n_Pos_Ini_3"]) - 1, Convert.ToInt32(row["n_Caracteres_3"])) + "0");
                                        PrecioBal = Convert.ToDouble(Math.Round(Convert.ToDouble((BsBal.ToString() + "." + DecBal.ToString())), 3));
                                    }
                                    else
                                        PrecioBal = Convert.ToDouble(Text1.Substring(Convert.ToInt32(row["n_Pos_Ini_2"]) - 1, Convert.ToInt32(row["n_Caracteres_2"])));

                                    break;

                                case 1: //**** PESO
                                    
                                    Cal_Precio = false;
                                    
                                    Text1 = Text1.Substring(Convert.ToInt32(row["n_Pos_Ini_1"]) - 1, Convert.ToInt32(row["n_Caracteres_1"]));
                
                                    EtiquetaProd = true;

                                    PesoBal = Convert.ToDouble(Codigo.Substring(Convert.ToInt32(row["n_Pos_Ini_2"]) - 1, Convert.ToInt32(row["n_Caracteres_2"])));
                                    PesoBalDec = Convert.ToDouble(Codigo.Substring(Convert.ToInt32(row["n_Pos_Ini_3"]) - 1, Convert.ToInt32(row["n_Caracteres_3"])) + "0");
                                    PesoBal = Convert.ToDouble(Math.Round(Convert.ToDouble((PesoBal.ToString() + "." + PesoBalDec.ToString())), 3));

                                    break;

                                case 2: //**** CARACTERISTICAS EXTENDIDAS
                                    
                                    EtiquetaProd = false;

                                    break;
                            }

                        }

                        Codigo = Text1;
                        codigoBox.Text = Codigo;

                    }

                    dtMatriz.Clear();
                    dtMatriz.Dispose();
                    daMatriz.Dispose();

                } // Fin Etiqueta
                
                SqlDataAdapter priceType = new SqlDataAdapter( 
                "SELECT CAST(MC.nu_TipoPrecio AS VARCHAR(10)) AS ColumnName " +
                "FROM MA_CODIGOS MC " + 
                "WHERE MC.c_Codigo = '" + fn.FixTSQL(Codigo) + "' ", connection);
                
                DataTable dtPriceType = new DataTable();
                Int32 ResPriceType = priceType.Fill(dtPriceType);
                
                String priceTypeStr = "0 ";

                if (ResPriceType > 0 && dtPriceType.Rows.Count > 0)
                {

                    priceTypeStr = dtPriceType.Rows[0]["columnName"].ToString() + " ";

                }

                priceType.Dispose();

                if (priceTypeStr.Equals("0 "))
                {

                    if (fn.TipoPrecioMostrar.Equals("0"))
                    {

                        priceType = new SqlDataAdapter(
                        "SELECT n_Precio FROM [" + fn.BD_ADM + "].[DBO].MA_CLIENTES " +
                        "WHERE c_CodCliente = (SELECT TOP 1 cu_Cliente_Contado " +
                        "FROM [" + fn.BD_ADM + "].[DBO].REGLAS_COMERCIALES) ", connection);

                        dtPriceType = new DataTable();

                        ResPriceType = priceType.Fill(dtPriceType);

                        if (ResPriceType > 0 && dtPriceType.Rows.Count > 0)
                        {
                            priceTypeStr = dtPriceType.Rows[0]["n_Precio"].ToString() + " ";
                        }

                        priceType.Dispose();

                    }
                    else
                    {
                        priceTypeStr = fn.TipoPrecioMostrar + " ";
                    }

                }
                
                //mJoinMoneda = String.Empty;
                mMultiplicadorCampos = String.Empty;

                mJoinMoneda = " LEFT JOIN [" + fn.BD_ADM + "].[DBO].[MA_MONEDAS] MON ON P.c_CodMoneda = MON.c_CodMoneda ";
                mFactorizarQuery = " * isNULL(CASE WHEN MON.n_Factor <> 0 THEN MON.n_Factor ELSE 1 END, 1)";

                if (FactorizarPrecios)
                {
                    mMultiplicadorCampos = mFactorizarQuery;
                }
                
                consultaCodigo = "SELECT P.c_Codigo, P.c_Descri, P.n_Impuesto1, ";
                
                if (fn.ImpresoraFiscal_DecimalesPrecio >= 0 && FactorizarPrecios)
                    consultaCodigo += 
                    "ROUND((P.n_Precio" + priceTypeStr + mMultiplicadorCampos + "), " + fn.ImpresoraFiscal_DecimalesPrecio.ToString() + ", 0) * C.n_Cantidad AS Precio1, " + 
                    "ROUND((P.n_PrecioO" + mMultiplicadorCampos + "), " + fn.ImpresoraFiscal_DecimalesPrecio.ToString() + ", 0) * C.n_Cantidad AS PrecioOf, " + 
                    "((ROUND((P.n_Precio" + priceTypeStr + mMultiplicadorCampos + "), " + fn.ImpresoraFiscal_DecimalesPrecio.ToString() + ", 0) * C.n_Cantidad) * (P.n_Impuesto1 / 100)) AS IVA, " + 
                    "((ROUND((P.n_Precio" + priceTypeStr + mMultiplicadorCampos + "), " + fn.ImpresoraFiscal_DecimalesPrecio.ToString() + ", 0) * C.n_Cantidad) * (1 + (P.n_Impuesto1 / 100))) AS TotalGral, " + 
                    "((ROUND((P.n_PrecioO" + mMultiplicadorCampos + "), " + fn.ImpresoraFiscal_DecimalesPrecio.ToString() + ", 0) * C.n_Cantidad) * (1 + (P.n_Impuesto1 / 100))) AS TotalOf, ";
                else
                    consultaCodigo += 
                    "(P.n_Precio" + priceTypeStr + mMultiplicadorCampos + ")* C.n_Cantidad AS Precio1, " +
                    "(P.n_PrecioO" + mMultiplicadorCampos + ") * C.n_Cantidad AS PrecioOf, " +
                    "(((P.n_Precio" + priceTypeStr + mMultiplicadorCampos + ")* C.n_Cantidad) * (P.n_Impuesto1 / 100)) AS IVA, " +
                    "(((P.n_Precio" + priceTypeStr + mMultiplicadorCampos + ")* C.n_Cantidad) * (1 + (P.n_Impuesto1 / 100))) AS TotalGral, " +
                    "(((P.n_PrecioO" + mMultiplicadorCampos + ")* C.n_Cantidad) * (1 + (P.n_Impuesto1 / 100))) AS TotalOf, ";

                consultaCodigo +=
                "P.f_Inicial, P.f_Final, P.h_Inicial, P.h_Final, " +
                "P.c_CodMoneda, isNULL(MON.c_Descripcion, 'N/A') AS c_MonedaDesc, " +
                "isNULL(MON.c_Simbolo, '') AS c_MonedaSimbolo, " +
                "isNULL(MON.n_Factor, 1) AS n_Factor, " +
                "p.n_Precio1 AS PrecioFicha, p.n_PrecioO AS PrecioOfertaFicha, " +
                "p.Cant_Decimales, isNULL(MON.n_Decimales, 2) AS DecMoneda, ";

                if (fn.ImpresoraFiscal_DecimalesPrecio >= 0)
                    consultaCodigo += 
                    "(ROUND((P.n_Precio" + priceTypeStr + mFactorizarQuery + "), " + fn.ImpresoraFiscal_DecimalesPrecio.ToString() + ", 0) * (1 + (P.n_Impuesto1 / 100))) AS TotalGralPred, " +
                    "(ROUND((P.n_PrecioO" + mFactorizarQuery + "), " + fn.ImpresoraFiscal_DecimalesPrecio.ToString() + ", 0)  * (1 + (P.n_Impuesto1 / 100))) AS TotalOfPred ";
                else
                    consultaCodigo += 
                    "((P.n_Precio" + priceTypeStr + mFactorizarQuery + ") * (1 + (P.n_Impuesto1 / 100))) AS TotalGralPred, " +
                    "((P.n_PrecioO" + mFactorizarQuery + ") * (1 + (P.n_Impuesto1 / 100))) AS TotalOfPred ";

                consultaCodigo += 
                "FROM MA_PRODUCTOS P " +
                "INNER JOIN MA_CODIGOS C ON P.c_Codigo = C.c_CodNasa " +
                mJoinMoneda +
                "WHERE C.c_Codigo = '" + fn.FixTSQL(Codigo) + "' "; // QuitarComillas

                da_codigo = new SqlDataAdapter(consultaCodigo, connection);
                dt_codigo = new DataTable();
                dt_codigo.Clear();

            }
            catch (SqlException sqle)
            {
                //MessageBox.Show(sqle.Message);
                var.escribirLogError(sqle.Message, "Consultar()");
                //sinConexion SC = new sinConexion();
                //SC.Show();
                //SC.Close();
                //SC = null;
                return;
            }
            catch (Exception Any)
            {
                var.escribirLogError(Any.Message, "Consultar()");
                return;
            }

            try
            {

                //serverDateTime = DateTime.Now;
                horaFechaServidor();

                // Chequear Moneda Predeterminada

                SqlDataAdapter daMoneda = new SqlDataAdapter(
                "SELECT TOP 1 * FROM " + fn.BD_ADM + ".DBO.MA_MONEDAS " +
                "WHERE b_Preferencia = 1 ", connection);
                DataTable dtMoneda = new DataTable(); ;
                Int32 ResMonedaPref = daMoneda.Fill(dtMoneda);

                if (ResMonedaPref > 0 && dtMoneda.Rows.Count > 0)
                {
                    fn.Moneda_Cod = dtMoneda.Rows[0]["c_CodMoneda"].ToString();
                    fn.Moneda_Des = dtMoneda.Rows[0]["c_Descripcion"].ToString();
                    fn.Moneda_Sim = dtMoneda.Rows[0]["c_Simbolo"].ToString();
                    fn.Moneda_Fac = Convert.ToDouble(dtMoneda.Rows[0]["n_Factor"]);
                    fn.Moneda_Dec = Convert.ToInt32(dtMoneda.Rows[0]["n_Decimales"]);
                }

                dtMoneda.Clear();
                dtMoneda.Dispose();
                daMoneda.Dispose();

                Int32 result2 = da_codigo.Fill(dt_codigo);

                if (result2 > 0)
                {

                    foreach (DataRow dr2 in dt_codigo.Rows)
                    {
                        Codigo = dr2[0].ToString();
                        descripcion = dr2[1].ToString();
                        tasa = Convert.ToDouble(dr2[2]);
                        subtotal = Convert.ToDouble(dr2[3]);
                        precioOferta = Convert.ToDouble(dr2[4]);
                        impuesto = Convert.ToDouble(dr2[5]);
                        precio = Convert.ToDouble(dr2[6]);
                        totalOferta = Convert.ToDouble(dr2[7]);
                        fInicio = (DateTime)dr2[8];
                        fFin = (DateTime)dr2[9];
                        hInicio = (DateTime)dr2[10];
                        hFin = (DateTime)dr2[11];
                        hCodMon = dr2[12].ToString();
                        hDesMon = dr2[13].ToString();
                        hSimbolo = dr2[14].ToString();
                        hFactor = Convert.ToDouble(dr2[15]);
                        fn.PrecioFicha = Convert.ToDouble(dr2["PrecioFicha"]);
                        fn.PrecioOfertaFicha = Convert.ToDouble(dr2["PrecioOfertaFicha"]);
                        fn.CantDec = Convert.ToInt32(dr2["Cant_Decimales"]);
                        fn.DecMonedaProd = Convert.ToInt32(dr2["DecMoneda"]);
                        PrecioTotalPred = Convert.ToDouble(dr2["TotalGralPred"]);
                        PrecioTotalOfertaPred = Convert.ToDouble(dr2["TotalOfPred"]);
                    }

                    if (EtiquetaProd)
                    {
                        
                        //Boolean EtiquetaInvalida = false;
                        
                        //if (Cal_Precio && PrecioBal == 0)
                        //    EtiquetaInvalida = true;
                        //else if (!Cal_Precio && PesoBal == 0)
                        //    EtiquetaInvalida = true;

                        //Console.WriteLine(EtiquetaInvalida);
                        
                        if (MaestroEtiquetaPrecioIncluyeIva && Cal_Precio)
                            PrecioBal = (PrecioBal / (1 + (tasa / 100D)));
                        
                        // Determinar Cantidad
                        
                        PrecioVenta = subtotal;
                        PrecioReal = PrecioVenta;
                        PrecioOferta = precioOferta;
                        
                        if ((serverDateTime.Date.CompareTo(fInicio.Date) >= 0)
                        && (serverDateTime.Date.CompareTo(fFin.Date) <= 0)
                        && (serverDateTime.Hour.CompareTo(hInicio.Hour) >= 0)
                        && (serverDateTime.Hour.CompareTo(hFin.Hour) < 0)
                        && (PrecioOferta != 0)
                        && (PrecioOferta < PrecioVenta) 
                        )
                            PrecioVenta = PrecioOferta;

                        if (PrecioReal == 0)
                            CantEtiq = 1;
                        else
                        {
                            if (Cal_Precio)
                                CantEtiq = Math.Round(PrecioBal / PrecioVenta, 8);
                            else
                                CantEtiq = PesoBal;
                        }
                        
                        Double OriginalCantEtiqueta;
                        
                        if (CantEtiq > 0)
                        {
                            //** ASEGURAR EL TIPO DE PRODUCTO
                            OriginalCantEtiqueta = CantEtiq;
                            CantEtiq = Math.Round(CantEtiq, fn.CantDec);
                        }
                        else
                        {
                            OriginalCantEtiqueta = 0;
                            CantEtiq = 1;
                        }

                        PrecioEtiq = PrecioVenta;
                        
                        // Subtotal
                        
                        if (EtiquetaProd)
                            if (Cal_Precio)
                            {
                                SubtotalEtiq = PrecioBal;
                                PrecioEtiq = Math.Round(SubtotalEtiq / CantEtiq, 8);
                            }
                        
                        SubtotalEtiq = (CantEtiq * PrecioEtiq);
                        
                    } // Fin Etiqueta

                    if (FactorizarPrecios)
                    {
                        var.SimMoneda = SimboloMoneda;
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(SimboloMoneda))
                            var.SimMoneda = hSimbolo;
                        else
                            var.SimMoneda = String.Empty;
                    }

                    fn.FactorizandoPrecios = FactorizarPrecios;

                    var.LabelTaxID = LabelTaxID;
                    var.LabelPrice = LabelPrice;
                    var.LabelPriceOffer = LabelPriceOffer;
                    var.LabelTotal = LabelTotal;
                    var.LabelFrom = LabelFrom;
                    var.LabelUntil = LabelUntil;
                    var.VarMostrarProductImgNotFound = MostrarProductImgNotFound;

                    Double PrecioFormateado;
                    String mDecimalesFmt;

                    if (fn.ImpresoraFiscal_DecimalesPrecio >= 0 && FactorizarPrecios)
                        mDecimalesFmt = (fn.ImpresoraFiscal_DecimalesPrecio == 0 ? String.Empty : "." + String.Empty.PadRight(fn.ImpresoraFiscal_DecimalesPrecio, '0'));
                    else
                        mDecimalesFmt = (fn.Moneda_Dec == 0 ? String.Empty : "." + String.Empty.PadRight(fn.Moneda_Dec, '0'));
                    
                    if (EtiquetaProd)
                    {

                        subtotal = SubtotalEtiq;

                        if ((PrecioOferta != 0) && (PrecioOferta < PrecioVenta))
                        {
                            precioOferta = subtotal;
                            tieneOferta = true;
                        }
                        else
                        {
                            precioOferta = 0;
                            tieneOferta = false;
                        }

                        impuesto = (subtotal * (0 + (tasa / 100D)));
                        precio = (subtotal + impuesto);
                        totalOferta = precio;

                        descripcion = "*ETIQUETA* " + descripcion;

                        var.receiveData(descripcion, impuesto, precioOferta, totalOferta,
                        subtotal, precio, fInicio, fFin, tieneOferta);

                        if (tieneOferta)
                            PrecioFormateado = Convert.ToDouble(totalOferta.ToString("######0" + mDecimalesFmt,
                            System.Globalization.CultureInfo.InvariantCulture));
                        else
                            PrecioFormateado = Convert.ToDouble(precio.ToString("######0" + mDecimalesFmt,
                            System.Globalization.CultureInfo.InvariantCulture));

                    }
                    else
                    {

                        if ((serverDateTime.Date.CompareTo(fInicio.Date) >= 0)
                        && (serverDateTime.Date.CompareTo(fFin.Date) <= 0)
                        && (serverDateTime.Hour.CompareTo(hInicio.Hour) >= 0)
                        && (serverDateTime.Hour.CompareTo(hFin.Hour) < 0))
                        {
                            tieneOferta = true;
                            var.receiveData(descripcion, impuesto, precioOferta, totalOferta,
                            subtotal, precio, fInicio, fFin, true);
                            PrecioFormateado = Convert.ToDouble(PrecioTotalOfertaPred.ToString("######0" + mDecimalesFmt,
                            System.Globalization.CultureInfo.InvariantCulture));
                        }
                        else
                        {
                            tieneOferta = false;
                            var.receiveData(descripcion, impuesto, precioOferta, totalOferta,
                            subtotal, precio, fechaInicio, fechaLimite, false);
                            PrecioFormateado = Convert.ToDouble(PrecioTotalPred.ToString("######0" + mDecimalesFmt,
                            System.Globalization.CultureInfo.InvariantCulture));
                        }

                    }

                    if (fn.Precios_ConsultaMonedaAlterna == "*")
                    {

                        dt_codigo.Clear();
                        dt_codigo.Dispose();
                        da_codigo.Dispose();

                        if (!hCodMon.Equals(fn.Moneda_Cod, StringComparison.OrdinalIgnoreCase))
                        {

                            consultaCodigo = "SELECT * FROM " + fn.BD_ADM + ".DBO.MA_MONEDAS " +
                            "WHERE c_CodMoneda = '" + fn.FixTSQL(hCodMon) + "' ";
                            dt_codigo = new DataTable();
                            da_codigo = new SqlDataAdapter(consultaCodigo, connection);

                            Int32 ResMonedaAlt = da_codigo.Fill(dt_codigo);

                            if (ResMonedaAlt > 0 && dt_codigo.Rows.Count > 0)
                            {

                                fn.MonedaAlt_Cod = dt_codigo.Rows[0]["c_CodMoneda"].ToString();
                                fn.MonedaAlt_Des = dt_codigo.Rows[0]["c_Descripcion"].ToString();
                                fn.MonedaAlt_Sim = dt_codigo.Rows[0]["c_Simbolo"].ToString();
                                fn.MonedaAlt_Fac = Convert.ToDouble(dt_codigo.Rows[0]["n_Factor"]);
                                if (fn.Precios_ConsultaMonedaAlterna_DecimalesPantalla >= 0)
                                    fn.MonedaAlt_Dec = fn.Precios_ConsultaMonedaAlterna_DecimalesPantalla;
                                else
                                    fn.MonedaAlt_Dec = Convert.ToInt32(dt_codigo.Rows[0]["n_Decimales"]);

                                if (EtiquetaProd)
                                {
                                    fn.PrecioFicha = (subtotal / fn.MonedaAlt_Fac);
                                    if (fn.Precios_ConsultaMonedaAlterna_RedondearHaciaArriba)
                                        fn.PrecioFinalAlt = (Convert.ToDouble(precio.ToString("######0" + mDecimalesFmt,
                                        System.Globalization.CultureInfo.InvariantCulture)) / fn.MonedaAlt_Fac);
                                    else
                                        fn.PrecioFinalAlt = (fn.PrecioFicha * (1 + (tasa / 100D)));
                                }
                                else
                                {
                                    if (fn.Precios_ConsultaMonedaAlterna_RedondearHaciaArriba)
                                    {

                                        if (tieneOferta)
                                            fn.PrecioFinalAlt = PrecioTotalOfertaPred;
                                        else
                                            fn.PrecioFinalAlt = PrecioTotalPred;

                                        fn.PrecioFinalAlt = (Convert.ToDouble(fn.PrecioFinalAlt.ToString("######0" + mDecimalesFmt,
                                        System.Globalization.CultureInfo.InvariantCulture)) / fn.MonedaAlt_Fac);

                                    }
                                    else
                                    {

                                        if (tieneOferta)
                                            fn.PrecioFinalAlt = fn.PrecioOfertaFicha;
                                        else
                                            fn.PrecioFinalAlt = fn.PrecioFicha;

                                        fn.PrecioFinalAlt = (fn.PrecioFinalAlt * (1 + (tasa / 100D)));
                                    }
                                }

                                fn.MostrarPrecioAlterno = true;

                            }

                        }

                    }
                    else if (!String.IsNullOrEmpty(fn.Precios_ConsultaMonedaAlterna)) 
                    {

                        dt_codigo.Clear();
                        dt_codigo.Dispose();
                        da_codigo.Dispose();

                        Boolean TmpMostrar = (FactorizarPrecios ? !fn.Precios_ConsultaMonedaAlterna.Equals(fn.Moneda_Cod, StringComparison.OrdinalIgnoreCase)
                        : (!fn.Precios_ConsultaMonedaAlterna.Equals(hCodMon, StringComparison.OrdinalIgnoreCase)));

                        if (TmpMostrar)
                        {

                            Double FacOrigen = -1;
                            Double DecOrigen = -1;

                            consultaCodigo = "SELECT * FROM " + fn.BD_ADM + ".DBO.MA_MONEDAS " +
                            "WHERE c_CodMoneda = '" + fn.FixTSQL(hCodMon) + "' ";
                            dt_codigo = new DataTable();
                            da_codigo = new SqlDataAdapter(consultaCodigo, connection);

                            Int32 ResMonedaAlt = da_codigo.Fill(dt_codigo);

                            if (ResMonedaAlt > 0 && dt_codigo.Rows.Count > 0)
                            {
                                FacOrigen = Convert.ToDouble(dt_codigo.Rows[0]["n_Factor"]);
                                DecOrigen = Convert.ToInt32(dt_codigo.Rows[0]["n_Decimales"]);
                            }

                            dt_codigo.Clear();
                            dt_codigo.Dispose();
                            da_codigo.Dispose();

                            consultaCodigo = "SELECT * FROM " + fn.BD_ADM + ".DBO.MA_MONEDAS " + 
                            "WHERE c_CodMoneda = '" + fn.FixTSQL(fn.Precios_ConsultaMonedaAlterna) + "' ";
                            dt_codigo = new DataTable();
                            da_codigo = new SqlDataAdapter(consultaCodigo, connection);

                            ResMonedaAlt = da_codigo.Fill(dt_codigo);

                            if (FacOrigen > 0 && ResMonedaAlt > 0 && dt_codigo.Rows.Count > 0)
                            {

                                fn.MonedaAlt_Cod = dt_codigo.Rows[0]["c_CodMoneda"].ToString();
                                fn.MonedaAlt_Des = dt_codigo.Rows[0]["c_Descripcion"].ToString();
                                fn.MonedaAlt_Sim = dt_codigo.Rows[0]["c_Simbolo"].ToString();
                                fn.MonedaAlt_Fac = Convert.ToDouble(dt_codigo.Rows[0]["n_Factor"]);
                                if (fn.Precios_ConsultaMonedaAlterna_DecimalesPantalla >= 0)
                                    fn.MonedaAlt_Dec = fn.Precios_ConsultaMonedaAlterna_DecimalesPantalla;
                                else
                                    fn.MonedaAlt_Dec = Convert.ToInt32(dt_codigo.Rows[0]["n_Decimales"]);

                                if (fn.Precios_ConsultaMonedaAlterna_RedondearHaciaArriba)
                                {

                                    if (EtiquetaProd)
                                    {
                                        fn.PrecioFinalAlt = (Convert.ToDouble(precio.ToString("######0" + mDecimalesFmt,
                                        System.Globalization.CultureInfo.InvariantCulture)) / fn.MonedaAlt_Fac);
                                    }
                                    else
                                    {

                                        if (tieneOferta)
                                            fn.PrecioFinalAlt = PrecioTotalOfertaPred;
                                        else
                                            fn.PrecioFinalAlt = PrecioTotalPred;

                                        fn.PrecioFinalAlt = (Convert.ToDouble(fn.PrecioFinalAlt.ToString("######0" + mDecimalesFmt,
                                        System.Globalization.CultureInfo.InvariantCulture)) / fn.MonedaAlt_Fac);

                                    }
                                }
                                else
                                {

                                    if (EtiquetaProd)
                                    {
                                        fn.PrecioFicha = (subtotal / fn.MonedaAlt_Fac);
                                        fn.PrecioFinalAlt = (fn.PrecioFicha * (1 + (tasa / 100D)));
                                    }
                                    else
                                    {
                                        if (tieneOferta)
                                            fn.PrecioFinalAlt = fn.PrecioOfertaFicha;
                                        else
                                            fn.PrecioFinalAlt = fn.PrecioFicha;

                                        fn.PrecioFinalAlt = (fn.PrecioFinalAlt * (1 + (tasa / 100D)));
                                    }

                                    fn.PrecioFinalAlt = (fn.PrecioFinalAlt * (FacOrigen / fn.MonedaAlt_Fac));

                                }

                                fn.MostrarPrecioAlterno = true;

                            }

                        }

                    }

                    if (fn.Precios_ConsultaMonedaAlterna2 == "*")
                    {

                        dt_codigo.Clear();
                        dt_codigo.Dispose();
                        da_codigo.Dispose();

                        if (!hCodMon.Equals(fn.Moneda_Cod, StringComparison.OrdinalIgnoreCase))
                        {

                            consultaCodigo = "SELECT * FROM " + fn.BD_ADM + ".DBO.MA_MONEDAS " + 
                            "WHERE c_CodMoneda = '" + fn.FixTSQL(hCodMon) + "' ";
                            dt_codigo = new DataTable();
                            da_codigo = new SqlDataAdapter(consultaCodigo, connection);

                            Int32 ResMonedaAlt = da_codigo.Fill(dt_codigo);

                            if (ResMonedaAlt > 0 && dt_codigo.Rows.Count > 0)
                            {

                                fn.MonedaAlt2_Cod = dt_codigo.Rows[0]["c_CodMoneda"].ToString();
                                fn.MonedaAlt2_Des = dt_codigo.Rows[0]["c_Descripcion"].ToString();
                                fn.MonedaAlt2_Sim = dt_codigo.Rows[0]["c_Simbolo"].ToString();
                                fn.MonedaAlt2_Fac = Convert.ToDouble(dt_codigo.Rows[0]["n_Factor"]);
                                if (fn.Precios_ConsultaMonedaAlterna2_DecimalesPantalla >= 0)
                                    fn.MonedaAlt2_Dec = fn.Precios_ConsultaMonedaAlterna2_DecimalesPantalla;
                                else
                                    fn.MonedaAlt2_Dec = Convert.ToInt32(dt_codigo.Rows[0]["n_Decimales"]);

                                if (EtiquetaProd)
                                {
                                    fn.PrecioFicha = (subtotal / fn.MonedaAlt2_Fac);
                                    if (fn.Precios_ConsultaMonedaAlterna_RedondearHaciaArriba)
                                        fn.PrecioFinalAlt2 = (Convert.ToDouble(precio.ToString("######0" + mDecimalesFmt,
                                        System.Globalization.CultureInfo.InvariantCulture)) / fn.MonedaAlt_Fac);
                                    else
                                        fn.PrecioFinalAlt2 = (fn.PrecioFicha * (1 + (tasa / 100D)));
                                }
                                else
                                {

                                    if (fn.Precios_ConsultaMonedaAlterna_RedondearHaciaArriba)
                                    {

                                        if (tieneOferta)
                                            fn.PrecioFinalAlt2 = PrecioTotalOfertaPred;
                                        else
                                            fn.PrecioFinalAlt2 = PrecioTotalPred;

                                        fn.PrecioFinalAlt2 = (Convert.ToDouble(fn.PrecioFinalAlt2.ToString("######0" + mDecimalesFmt,
                                        System.Globalization.CultureInfo.InvariantCulture)) / fn.MonedaAlt_Fac);

                                    }
                                    else
                                    {

                                        if (tieneOferta)
                                            fn.PrecioFinalAlt2 = fn.PrecioOfertaFicha;
                                        else
                                            fn.PrecioFinalAlt2 = fn.PrecioFicha;

                                        fn.PrecioFinalAlt2 = (fn.PrecioFinalAlt2 * (1 + (tasa / 100D)));
                                    }
                                }

                                fn.MostrarPrecioAlterno2 = true;
                            }

                        }

                    }
                    else if (!String.IsNullOrEmpty(fn.Precios_ConsultaMonedaAlterna2))
                    {

                        dt_codigo.Clear();
                        dt_codigo.Dispose();
                        da_codigo.Dispose();

                        Boolean TmpMostrar = (FactorizarPrecios ? !fn.Precios_ConsultaMonedaAlterna2.Equals(fn.Moneda_Cod, StringComparison.OrdinalIgnoreCase)
                        : (!fn.Precios_ConsultaMonedaAlterna2.Equals(hCodMon, StringComparison.OrdinalIgnoreCase)));

                        if (TmpMostrar)
                        {

                            Double FacOrigen = -1;
                            Double DecOrigen = -1;

                            consultaCodigo = "SELECT * FROM " + fn.BD_ADM + ".DBO.MA_MONEDAS " +
                            "WHERE c_CodMoneda = '" + fn.FixTSQL(hCodMon) + "' ";
                            dt_codigo = new DataTable();
                            da_codigo = new SqlDataAdapter(consultaCodigo, connection);

                            Int32 ResMonedaAlt = da_codigo.Fill(dt_codigo);

                            if (ResMonedaAlt > 0 && dt_codigo.Rows.Count > 0)
                            {
                                FacOrigen = Convert.ToDouble(dt_codigo.Rows[0]["n_Factor"]);
                                DecOrigen = Convert.ToInt32(dt_codigo.Rows[0]["n_Decimales"]);
                            }

                            dt_codigo.Clear();
                            dt_codigo.Dispose();
                            da_codigo.Dispose();

                            consultaCodigo = "SELECT * FROM " + fn.BD_ADM + ".DBO.MA_MONEDAS " + 
                            "WHERE c_CodMoneda = '" + fn.FixTSQL(fn.Precios_ConsultaMonedaAlterna2) + "' ";
                            dt_codigo = new DataTable();
                            da_codigo = new SqlDataAdapter(consultaCodigo, connection);

                            ResMonedaAlt = da_codigo.Fill(dt_codigo);

                            if (FacOrigen > 0 && ResMonedaAlt > 0 && dt_codigo.Rows.Count > 0)
                            {

                                fn.MonedaAlt2_Cod = dt_codigo.Rows[0]["c_CodMoneda"].ToString();
                                fn.MonedaAlt2_Des = dt_codigo.Rows[0]["c_Descripcion"].ToString();
                                fn.MonedaAlt2_Sim = dt_codigo.Rows[0]["c_Simbolo"].ToString();
                                fn.MonedaAlt2_Fac = Convert.ToDouble(dt_codigo.Rows[0]["n_Factor"]);
                                if (fn.Precios_ConsultaMonedaAlterna2_DecimalesPantalla >= 0)
                                    fn.MonedaAlt2_Dec = fn.Precios_ConsultaMonedaAlterna2_DecimalesPantalla;
                                else
                                    fn.MonedaAlt2_Dec = Convert.ToInt32(dt_codigo.Rows[0]["n_Decimales"]);

                                if (fn.Precios_ConsultaMonedaAlterna_RedondearHaciaArriba)
                                {

                                    if (EtiquetaProd)
                                    {
                                        fn.PrecioFinalAlt2 = (Convert.ToDouble(precio.ToString("######0" + mDecimalesFmt,
                                        System.Globalization.CultureInfo.InvariantCulture)) / fn.MonedaAlt2_Fac);
                                    }
                                    else
                                    {

                                        if (tieneOferta)
                                            fn.PrecioFinalAlt2 = PrecioTotalOfertaPred;
                                        else
                                            fn.PrecioFinalAlt2 = PrecioTotalPred;

                                        fn.PrecioFinalAlt2 = (Convert.ToDouble(fn.PrecioFinalAlt2.ToString("######0" + mDecimalesFmt,
                                        System.Globalization.CultureInfo.InvariantCulture)) / fn.MonedaAlt2_Fac);

                                    }
                                }
                                else
                                {

                                    if (EtiquetaProd)
                                    {
                                        fn.PrecioFicha = (subtotal / fn.MonedaAlt2_Fac);
                                        fn.PrecioFinalAlt2 = (fn.PrecioFicha * (1 + (tasa / 100D)));
                                    }
                                    else
                                    {
                                        if (tieneOferta)
                                            fn.PrecioFinalAlt2 = fn.PrecioOfertaFicha;
                                        else
                                            fn.PrecioFinalAlt2 = fn.PrecioFicha;

                                        fn.PrecioFinalAlt2 = (fn.PrecioFinalAlt2 * (1 + (tasa / 100D)));
                                    }

                                    fn.PrecioFinalAlt2 = (fn.PrecioFinalAlt2 * (FacOrigen / fn.MonedaAlt2_Fac));
                                }

                                fn.MostrarPrecioAlterno2 = true;

                            }

                        }

                    }

                    detallesProducto detalles = new detallesProducto();
                    detalles.ShowDialog();
                    //detalles.Close();
                    detalles = null;

                }
                else
                {
                    productoNoRegistrado noReg = new productoNoRegistrado();
                    noReg.ShowDialog();
                    //noReg.Close();
                    noReg = null;
                }

            }
            catch (SqlException sqle)
            {
                var.escribirLogError(sqle.Message, "Consultar()");
                sinConexion SC = new sinConexion();
                SC.Show();
                //SC.Close();
                SC = null;
            }
            catch (NullReferenceException nre)
            {
                var.escribirLogError(nre.Message, "Consultar()");
                sinConexion SC = new sinConexion();
                SC.Show();
                //SC.Close();
                SC = null;
            }
            catch (InvalidCastException ice)
            {
                var.escribirLogError(ice.Message, "Consultar()");
            }
            catch (FormatException fe)
            {
                var.escribirLogError(fe.Message, "Consultar()");
            }
            catch (Exception idk)
            {
                var.escribirLogError(idk.Message, "Consultar()");
            }
            finally
            {

                da_codigo.Dispose();
                dt_codigo.Clear();
                dt_codigo.Dispose();

                if (connection.State != ConnectionState.Closed)
                {
                    connection.Close();
                }

                GC.Collect();

            }

            codigoBox.Text = "";

            if (EtiquetaProd && PostConsultaEtiquetaPrecioUnitario)
            {
                Consultar(Codigo);
                return;
            }

        }

        //Manejar datos obtenidos del lector
        private void HandleContinuousData(ScanData scanData)
        {
            Consultar(scanData.Text);
        }

        private void mainMenu_Activated(object sender, EventArgs e)
        {
            this.scanAPI.StartScan(true);
        }

        private void mainMenu_Deactivate(object sender, EventArgs e)
        {
            scanAPI.StopScan();
        }

        [DllImport("coredll.dll")]
        private static extern int SendMessageW(int hWnd, uint wMsg, int wParam, ref Point lParam);

        [DllImport("coredll.dll")]
        private static extern int SendMessageW(int hWnd, int wMsg, int wParam, int lParam);

        private void probarConexion_Click(object sender, EventArgs e)
        {
            Conexion connect = new Conexion();
            connect.consulta();
        }

        private void flechaTimer_Tick(object sender, EventArgs e)
        {

            if (flechaBox.Visible)
            {
                flechaBox.Hide();
            }
            else
            {
                flechaBox.Show();
            }

        }

        private void horaFechaServidor()
        {
            try
            {
                String consultaDate = "SELECT GetDate()";
                da_serverDate = new SqlDataAdapter(consultaDate, connection);
                dt_serverDate = new DataTable();
                dt_serverDate.Clear();
                int result5 = da_serverDate.Fill(dt_serverDate);
                if (result5 > 0)
                {
                    foreach (DataRow dr5 in dt_serverDate.Rows)
                    {
                        serverDateTime = (DateTime)dr5[0];
                    }
                }
            }
            catch (Exception e)
            {
                var.escribirLogError(e.Message, "horaFechaServidor");
            }
            finally
            {
                da_serverDate.Dispose();
                dt_serverDate.Clear();
                dt_serverDate.Dispose();
            }
        }

        private void ImgTimer_Tick(object sender, EventArgs e)
        {
            if (this.Visible)
                ChangeIMG();
        }

        private void codigoBox_KeyDown(object sender, KeyEventArgs e)
        {

            if (fn.TestMode)
            {
                if (e.KeyCode == Keys.Return)
                    Consultar(codigoBox.Text);
            }

        }

        private void codigoBox_TextChanged(object sender, EventArgs e)
        {

        }

    }

}
